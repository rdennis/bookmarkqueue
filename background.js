var bookmarkVisits = {};

function initializeVisits(map, tree) {
	if(tree.length > 0)
		for(var i=0; i<tree.length; ++i)
			initializeVisits(map, tree[i]);
	if(tree.children && tree.children.length > 0)
		for(var i=0; i<tree.children.length; ++i)
			initializeVisits(map, tree.children[i]);
	else
		if(tree.url)
			chrome.history.getVisits({url:tree.url}, function(result){map[tree.id]=result.length;});
}

chrome.runtime.onInstalled.addListener(function(details) {
	chrome.bookmarks.getTree(function(tree){initializeVisits(bookmarkVisits, tree);});
});

chrome.history.onVisited.addListener(function(result) {
	for(var bookmark in bookmarkVisits) {
		chrome.bookmarks.get(bookmark, function(res){
			if(res[0].url === result.url) {
				console.log(bookmark + ": " + bookmarkVisits[bookmark]++);
			}
		});
	}
});